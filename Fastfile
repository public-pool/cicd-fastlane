# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

build_json = read_json(json_path: "./deploy/fastlane.json")

def buildFlags(flavor) 
    out = "obfuscate/#{flavor}"
    sh("mkdir -p ../#{out}")
    flags = "--release --flavor #{flavor} --obfuscate --split-debug-info=./#{out}"
    return flags
end

def upload(firebase_config, config, isAndroid)
    group = firebase_config[:group]
    token = ENV[firebase_config[:token]]
    firebase = config[:firebase]
    flavor = config[:flavor]

    if firebase.length
        archives = archiveDestination(config, isAndroid)

        git_changelogs = changelog_from_git_commits(
            pretty: "%s",# Optional, lets you provide a custom format to apply to each commit when generating the changelog text
            date_format: "short",# Optional, lets you provide an additional date format to dates within the pretty-formatted string
            commits_count: 10,
        )      
        release_note_file = "release_note.txt"
        File.write("#{release_note_file}" , git_changelogs)

        archives.each { |archive|
            sh("firebase appdistribution:distribute #{archive} --token #{token} --app #{firebase} --groups #{group} --release-notes-file #{release_note_file}")
        }
        sh("rm #{release_note_file}")

        if isAndroid 
            # https://github.com/firebase/firebase-tools/issues/5291
            obfuscationDir = "../obfuscate/#{flavor}"
            mappings = sh("ls #{obfuscationDir}")
            mappings = mappings.split("\n")
            mappings.each { |mapping|
                target = isAndroid ? "android" : "ios"
                if mapping.include? target
                    symbols = "#{obfuscationDir}/#{mapping}"
                    sh("firebase crashlytics:symbols:upload #{symbols} --token #{token} --app #{firebase}")
                end
            }
        end
    end
end

def buildApp(config, isAndroid)
    flavor = config[:flavor]
    if isAndroid
      sh("flutter build apk #{buildFlags(flavor)}")
    else
      sh("flutter build ios --no-codesign #{buildFlags(flavor)}")
      gym(
        scheme: flavor,
        workspace: "./ios/Runner.xcworkspace",
        output_directory: "./build/ipa/",
        output_name: "#{flavor}.ipa",
        export_options: { method: config[:method] || "ad-hoc" },
        xcargs: "-allowProvisioningUpdates"
    )
    end
end
  
def archiveDestination(config, isAndroid) 
    flavor = config[:flavor]
    if isAndroid
        return [
            "../build/app/outputs/flutter-apk/app-#{flavor}-release.apk",
        ]
    else 
      return [
        "../build/ipa/#{flavor}.ipa"
      ]
    end
end  

lane :android do
    isAndroid = true
    androidBuilds = build_json[:android]
    androidBuilds.each { |build|
        buildApp(build, isAndroid)
        upload(build_json[:firebase], build, isAndroid)
    }

    if build_json[:slack] != ""
        links = androidBuilds.map { |build| "#{build[:ota]}" }.join("\n")
        slack(message: links, slack_url: build_json[:slack])
    end

end

lane :ios do
    isAndroid = false
    
    match_ios_certificates

    iosBuilds = build_json[:ios]
    iosBuilds.each { |build|
        buildApp(build, isAndroid)
        upload(build_json[:firebase], build, isAndroid)
    }

    if build_json[:slack] != ""
        links = iosBuilds.map { |build| "#{build[:ota]}" }.join("\n")
        slack(message: links, slack_url: build_json[:slack])
    end
end

lane :build do
    android
    ios
end

lane :google_play do
    sh("flutter build appbundle #{buildFlags("prod")}")
    upload_to_play_store(
        package_name: build_json[:deploy][:package_name],
        aab: "./build/app/outputs/bundle/prodRelease/app-prod-release.aab",
        json_key: "./deploy/google.json",
        track: "internal",
    )
end

lane :app_store do
    version = flutter_version(
        pubspec_location: "./pubspec.yaml"
    )

    sh("flutter build ios --no-codesign #{buildFlags("prod")}")

    gym(
        scheme: "prod",
        workspace: "./ios/Runner.xcworkspace",
        output_directory: "./build/ipa/",
        output_name: "app-store.ipa",
        export_options: { method: "app-store" },
        xcargs: "-allowProvisioningUpdates"
    )

    api_key = app_store_connect_api_key(
        key_id: build_json[:deploy][:key_id],
        issuer_id: build_json[:deploy][:issuer_id],
        key_filepath: "./deploy/apple.p8",
    )

    upload_to_testflight(
        api_key: api_key,
        ipa: "./build/ipa/app-store.ipa",
        app_version: version[:version_name],
        build_number: version[:version_code],
        skip_submission: true,
        skip_waiting_for_build_processing: true
    )
end

lane :match_ios_certificates do
    if build_json[:match]
        git_url = build_json[:match][:git_url]
        username = build_json[:match][:username]
        team_id = build_json[:match][:team_id]
        certificates = build_json[:match][:certificates]

        certificates.each { |certificate|
            match(
                type: certificate[:type],
                git_branch: certificate[:git_branch],
                app_identifier: certificate[:app_identifier],

                git_url: certificate[:git_url] || git_url,
                username: certificate[:username] || username,
                team_id: certificate[:team_id] || team_id,
                readonly: true,
            )
        }
    end
end

lane :deploy do
    google_play
    app_store
end
